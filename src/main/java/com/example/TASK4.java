package com.example;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Create an implementation of a Rest API client. Prints out how many records
 * exists for each gender and save this file to s3 bucket API endpoint=>
 * https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda AWS s3
 * bucket => interview-digiage
 * 
 * 
 * Parcialmente resolvido, ainda não possuo proficiência no AWS.  
 */
public class TASK4 {
	
	public static void main(String[] args) throws IOException {
		
	int n , cont = 0;
	
	try{		
		URL url =  new URL("https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda");
		HttpURLConnection conexao = (HttpURLConnection)url.openConnection();
		
		InputStream inputStream = conexao.getInputStream();
		
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		
		BufferedReader reader = new BufferedReader(inputStreamReader);	
        
		String result = reader.readLine();
		
		 n = result.length();
		 
		 for (int i=0; i<n; i++) {
		      if ((result.charAt(i) == 'M') || (result.charAt(i) == 'F')) {
		         cont++;
		         
		      }
		    }
		 System.out.println("São "+cont+" Registros");
		 
		
		}
	catch(MalformedURLException e)
		{
			e.printStackTrace();
		}
	catch(IOException e2){
		e2.printStackTrace();
	}
	
	
}



	
	
}
