package com.example;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {

	static boolean isPalRec(String str, int s, int e) {

		if (s == e)
			return true;

		if ((str.charAt(s)) != (str.charAt(e)))
			return false;

		if (s < e + 1)
			return isPalRec(str, s + 1, e - 1);

		return true;
	}

	static boolean isPalindrome(String str) {
		int n = str.length();

		if (n == 0)
			return true;

		return isPalRec(str, 0, n - 1);
	}

	// Driver Code
	public static void main(String[] args) {
		String str = "Marcos";
		//String str = "Natan";
		

		if (isPalindrome(str))
			System.out.println("Yes palindrome");
		else
			System.out.println("No palindrome");

	}

}
