package com.example;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {
	
    ArrayList<String> lista = new ArrayList<String>();
	
  
	public void adicionar(String valor) {
		lista.add(valor);
	}
   
	public void ordenar(){
		Collections.sort(lista);
	}
	
	
	public void imprimir() {
		int count = 0;
		for(int i=0;i<lista.size()-1;i++){
		if(lista.get(i).equals(lista.get(i+1))){
						
		}else{
			count++;
		}
		}
		if(count==0){
			System.out.println(lista+""+"Só existe "+(count+1)+" tipo de String");
		}else{
			System.out.println(lista+""+"O numero de strings distintas são: "+(count+1));
		}
		
	}
	
	public static void main(String[] args) {
		TASK3 task3 = new TASK3();
		
		task3.adicionar("Carro");
		task3.adicionar("Casa");
		task3.adicionar("Moto");
		task3.adicionar("Carro");
		task3.ordenar();
		
		task3.imprimir();
		
	}
	
	
}
