/*1- Query que retorna a quantidade de funcion�rios separados por sexo.*/

select gender as sexo, 
count(gender) as quantidade 

from employees group by gender 

/*2- Query que retorna a quantidade de funcion�rios distintos por sexo, ano e ano de nascimento.*/

select gender,
year(birth_date) as ano_nascimento, 
count(year(birth_date)) as qtd_funcionarios_ano_nascimento, 
year(hire_date) as ano_contratacao,
count(year(hire_date)) as qtd_funcioanarios_ano_contratacao
 
from employees group by ano_nascimento, ano_contratacao, 
gender

/*3- Query que retorna a m�dia, min e max de sal�rio por sexo.*/

select employees.gender as sexo, 
min(salary) as salario_minimo, 
avg(salary) as salario_medio, 
max(salary) as salario_maximo 

from salaries 
inner join employees using (emp_no) 
group by(employees.gender);
